import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Sidebar from './Sidebar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import AppStyles from '../styles/AppStyles';

class App extends Component {
	render() {
		return (
			<Router>
				<Fragment>
					<MuiThemeProvider theme={AppStyles}>
						<CssBaseline />
						<Sidebar />
						{/* <Footer /> */}
					</MuiThemeProvider>
				</Fragment>
			</Router>
		);
	}
}

export default App;
