import React, { Component } from 'react';
import { Route, Link } from 'react-router-dom';
import MedicamentosWrapper from '../components/Medicamentos/MedicamentosWrapper';
import Ingestas from './Ingestas';
import styles from '../styles/SidebarStyles';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import InvertColorsTwoTone from '@material-ui/icons/InvertColorsTwoTone';
import DoneAllOutlined from '@material-ui/icons/DoneAllOutlined';
import HomeTwoTone from '@material-ui/icons/HomeTwoTone';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import classNames from 'classnames';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

const routes = [
	{
		path: '/',
		exact: true,
		main: () => <div>sdfsdfsdf</div>,
		icon: <HomeTwoTone />,
		label: 'Home'
	},
	{
		path: '/medicamentos',
		main: () => <MedicamentosWrapper />,
		icon: <InvertColorsTwoTone />,
		label: 'Medicamentos'
	},
	{
		path: '/ingestas',
		main: () => <Ingestas />,
		icon: <DoneAllOutlined />,
		label: 'Ingestas'
	}
];

class Sidebar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			checked: true,
			open: false
		};
	}

	handleDrawerOpen = () => {
		this.setState({ open: true });
	};

	handleDrawerClose = () => {
		this.setState({ open: false });
	};

	render() {
		const { classes, theme } = this.props;

		return (
			<div className={classes.root}>
				<CssBaseline />
				<AppBar
					position="fixed"
					className={classNames(classes.appBar, {
						[classes.appBarShift]: this.state.open
					})}
				>
					<Toolbar disableGutters={!this.state.open}>
						<IconButton
							color="inherit"
							aria-label="Open drawer"
							onClick={this.handleDrawerOpen}
							className={classNames(classes.menuButton, {
								[classes.hide]: this.state.open
							})}
						>
							<MenuIcon />
						</IconButton>
						<Typography variant="h6" color="inherit" noWrap>
							Don't Forget your Meds
						</Typography>
					</Toolbar>
				</AppBar>

				<Drawer
					variant="permanent"
					className={classNames(classes.drawer, {
						[classes.drawerOpen]: this.state.open,
						[classes.drawerClose]: !this.state.open
					})}
					classes={{
						paper: classNames(classes.drawerPaper, {
							[classes.drawerOpen]: this.state.open,
							[classes.drawerClose]: !this.state.open
						})
					}}
					open={this.state.open}
				>
					<div className={classes.toolbar}>
						<IconButton onClick={this.handleDrawerClose}>
							{theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
						</IconButton>
					</div>
					<Divider />
					<MenuList>
						{routes.map((text, index) => (
							<MenuItem key={index} component={Link} to={text.path}>
								<ListItemIcon>{text.icon}</ListItemIcon>
								<ListItemText primary={text.label} />
							</MenuItem>
						))}
					</MenuList>
					<Divider />
				</Drawer>

				<main className={classes.content}>
					<div className={classes.toolbar} />
					<Grid container className={classes.containerDiv} spacing={16}>
						<Grid item xs={12}>
							<Paper className={classes.control}>
								<Grid container>
									{routes.map((route) => (
										<Route
											key={route.path}
											path={route.path}
											exact={route.exact}
											component={route.main}
										/>
									))}
								</Grid>
							</Paper>
						</Grid>
					</Grid>
				</main>
			</div>
		);
	}
}

Sidebar.propTypes = {
	classes: PropTypes.object.isRequired,
	theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(Sidebar);
