import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';
import React, { Component } from 'react';

class Footer extends Component {
	render() {
		const { classes } = this.props;
		const currentYear = new Date().getFullYear();

		return (
			<div className={classes.root}>
				<Grid container spacing={0} className={classNames(classes.footerText, classes.footerSections)}>
					<Grid item xs={12} sm={4}>
						<div vocab="http://schema.org/" typeof="Organization">
							<span property="name">Franciscan University of Steubenville</span>
							<div property="address" typeof="PostalAddress">
								<span property="streetAddress">1235 University Boulevard</span>
								<span property="addressLocality" style={{ display: 'block' }}>
									Steubenville, Ohio{' '}
								</span>
								<span property="postalCode">43952</span>
							</div>
							<span property="telephone">(740) 283-3771</span>
						</div>
					</Grid>
					<Grid item xs={12} sm={4}>
						<Grid container>
							<Grid className={classes.flexContainer} style={{ justifyContent: 'center' }} item xs={12} />
							<Grid
								className={classes.flexContainer}
								style={{ justifyContent: 'flex-end' }}
								item
								xs={6}
							/>
							<Grid className={classes.flexContainer} item xs={6} />
						</Grid>
					</Grid>
				</Grid>
				<Grid className={classes.subFooter} item xs={12}>
					<Typography className={classes.white} variant="subtitle1" component={'span'}>
						© {currentYear} Franciscan University of Steubenville
					</Typography>
				</Grid>
			</div>
		);
	}
}

const styles = (theme) => ({
	root: {
		marginTop: 30,
		backgroundColor: `${theme.palette.primary[500]}`,
		borderTop: 'solid 3px #998643',
		paddingTop: '16px',
		overflowX: 'hidden'
	},
	footerSections: {
		margin: '0 16px'
	},
	subFooter: {
		backgroundColor: 'rgba(0, 0, 0, 0.15)',
		padding: '8px 16px 8px 16px',
		marginTop: '8px'
	},
	footerText: {
		color: '#fff',
		fontSize: '18px',
		lineHeight: 1.5
	},
	invertedBtnDark: {
		color: '#fff',
		backgroundColor: 'transparent',
		border: '2px #fff solid',
		boxShadow: 'none',
		margin: '8px'
	},
	white: {
		color: '#ffffff'
	},
	flexContainer: {
		display: 'flex'
	},
	typography: {
		useNextVariants: true
	}
});

export default withStyles(styles)(Footer);
