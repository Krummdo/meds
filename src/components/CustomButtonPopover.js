import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import styles from '../styles/CustomButtonPopoverStyles';
import Button from '@material-ui/core/Button';

class CustomButtonPopover extends React.Component {
	state = {
		anchorEl: null
	};

	handlePopoverOpen = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handlePopoverClose = () => {
		this.setState({ anchorEl: null });
	};

	render() {
		const { classes, showText } = this.props;
		const { anchorEl } = this.state;
		const open = Boolean(anchorEl);
		return (
			<Fragment>
				<Button
					aria-owns={open ? 'mouse-over-popover' : undefined}
					aria-haspopup="true"
					onClick={this.props.actionButton}
					onMouseEnter={this.handlePopoverOpen}
					onMouseLeave={this.handlePopoverClose}
					variant="fab"
					color="secondary"
					aria-label="Edit"
					className={classes.button}
				>
					{this.props.iconButton}
				</Button>
				<Popover
					id="mouse-over-popover"
					className={classes.popover}
					classes={{
						paper: classes.paper
					}}
					open={open}
					anchorEl={anchorEl}
					anchorOrigin={{
						vertical: 'top',
						horizontal: 'center'
					}}
					transformOrigin={{
						vertical: 'center',
						horizontal: 'center'
					}}
					onClose={this.handlePopoverClose}
					disableRestoreFocus
				>
					<Typography>{showText}</Typography>
				</Popover>
			</Fragment>
		);
	}
}

CustomButtonPopover.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomButtonPopover);
