import React, { Fragment } from 'react';
import NumberFormat from 'react-number-format';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';

const styles = (theme) => ({
	container: {
		display: 'flex',
		flexWrap: 'wrap'
	},
	formControl: {
		marginRight: theme.spacing.unit
	},
	textField: {
		marginLeft: theme.spacing.unit,
		marginRight: theme.spacing.unit,
		width: 50
	}
});

function NumberFormatCustom(props) {
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			getInputRef={inputRef}
			onValueChange={(values) => {
				onChange({
					target: {
						value: values.value
					}
				});
			}}
			thousandSeparator
		/>
	);
}

NumberFormatCustom.propTypes = {
	inputRef: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired
};

class FormattedInputs extends React.Component {
	state = {
		numberformat: ''
	};

	handleChange = (name) => (event) => {
		this.setState({
			[name]: event.target.value
		});
	};

	render() {
		const { classes } = this.props;

		return (
			<Fragment>
				<TextField
					required
					className={classes.formControl}
					label={this.props.title}
					value={this.props.precio}
					onChange={this.handleChange('numberformat')}
					id="formatted-numberformat-input"
					InputProps={{
						inputComponent: NumberFormatCustom,
						startAdornment: <InputAdornment position="start">{this.props.prefijo}</InputAdornment>
					}}
				/>
			</Fragment>
		);
	}
}

FormattedInputs.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(FormattedInputs);
