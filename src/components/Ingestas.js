import React, { Component, Fragment } from 'react';
import CustomButtonPopover from './CustomButtonPopover';
import API from '../router/apiCentral.js';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import SaveIcon from '@material-ui/icons/Save';
import { styles, CustomTableCell } from '../styles/IngestasStyles';

class Ingestas extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ingestas: [],
			cabecera: []
		};
	}

	componentDidMount() {
		this.fetchData();
	}

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	fetchData() {
		const myApi = new API();
		myApi.createEntity({ name: 'medicamentos' });
		myApi.createEntity({ name: 'ingestas' });

		myApi.endpoints.medicamentos
			.getAll('')
			.then(({ data }) => {
				let cabecera = data;
				this.setState({ cabecera });
			})
			.catch((error) => console.log('skereeeeeeeeeeeeee', error));

		myApi.endpoints.ingestas
			.getAll('')
			.then(({ data }) => {
				let ingestas = data;
				this.setState({ ingestas });
			})
			.catch((error) => console.log('skereeeeeeeeeeeeee', error));
	}

	handleChange = (rowId, checkboxId) =>
		this.setState({
			ingestas: this.state.ingestas.map((ingesta, index) => {
				var medicamentos;
				if (index === rowId - 1) {
					//mirar
					medicamentos = ingesta.medicamentos.map((medicamento, index) => {
						if (medicamento.idmedicamento === checkboxId) {
							return {
								...medicamento,
								consumido: !medicamento.consumido
							};
						}
						return medicamento;
					});
					return {
						...ingesta,
						medicamentos: medicamentos
					};
				}
				return ingesta;
			})
		});

	render() {
		const { classes } = this.props;

		return (
			<Fragment>
				<Typography variant="h3" gutterBottom>
					Ingestas
				</Typography>
				<Paper className={classes.root}>
					<Table className={classes.table}>
						<TableHead>
							<TableRow>
								<CustomTableCell>Fecha</CustomTableCell>
								{this.state.cabecera.map((row, index) => (
									<CustomTableCell key={index}>{row.medName}</CustomTableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{this.state.ingestas.map((row, index) => {
								return (
									<TableRow className={classes.row} key={index}>
										<CustomTableCell component="th" scope="row">
											{row.fecha}
										</CustomTableCell>
										{row.medicamentos.map((rowMeds, index) => (
											<CustomTableCell key={index}>
												<Checkbox
													checked={rowMeds.consumido}
													onChange={(e) => this.handleChange(row.id, rowMeds.idmedicamento)}
												/>
											</CustomTableCell>
										))}
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Paper>
				<Grid container className={classes.root} spacing={16}>
					<Grid item xs={12}>
						<Grid container className={classes.demo} justify="flex-end" spacing={8}>
							<Grid item>
								<CustomButtonPopover showText="Save" iconButton={<SaveIcon />} />
							</Grid>
						</Grid>
					</Grid>
				</Grid>
			</Fragment>
		);
	}
}

Ingestas.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Ingestas);
