import React, { Fragment } from 'react';
import TextNumerico from '../TextNumerico';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import styles from '../../styles/NuevoMedicamentoStyles';

class NuevoMedicamento extends React.Component {
	handleInputChange = (event) => {
		const target = event.target;
		const name = target.name;

		this.setState({
			[name]: event.target.value
		});
	};

	render() {
		return (
			<Fragment>
				<Dialog open={this.props.open} onClose={this.props.handleClose} aria-labelledby="form-dialog-title">
					<DialogTitle id="form-dialog-title">Nuevo Medicamento</DialogTitle>
					<DialogContent>
						<DialogContentText>
							Complete los campos obligatorios para cargar un nuevo medicamento.
						</DialogContentText>
						<TextField
							required
							margin="dense"
							name="medName"
							label="Nombre"
							type="email"
							fullWidth
							onChange={this.handleInputChange}
						/>
						<TextNumerico title="Precio" name="precio" prefijo={'$'} onChange={this.handleInputChange} />
						<TextNumerico title="Stock" name="stock" prefijo={'Cant.'} onChange={this.handleInputChange} />
					</DialogContent>
					<DialogActions>
						<Button onClick={this.props.handleClose} color="primary">
							Cancel
						</Button>
						<Button onClick={this.props.save} color="primary">
							Save
						</Button>
					</DialogActions>
				</Dialog>
			</Fragment>
		);
	}
}

NuevoMedicamento.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NuevoMedicamento);
