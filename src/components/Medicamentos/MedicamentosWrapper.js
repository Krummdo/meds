import React, { Fragment } from 'react';
import Medicamentos from './Medicamentos';
import { MedicamentoContext, MedicamentoProvider } from '../../contexts/MedicamentosContext';

export default (props) => (
	<Fragment>
		<MedicamentoProvider>
			<MedicamentoContext.Consumer>
				{(value) => <Medicamentos {...props} {...value} />}
			</MedicamentoContext.Consumer>
		</MedicamentoProvider>
	</Fragment>
);
