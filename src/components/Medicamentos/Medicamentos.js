import React, { Component, Fragment } from 'react';
import NuevoMedicamento from './NuevoMedicamento';
import CustomButtonPopover from '../CustomButtonPopover';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SaveIcon from '@material-ui/icons/Save';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Checkbox from '@material-ui/core/Checkbox';
import { styles, CustomTableCell } from '../../styles/MedicamentosStyles';

class Medicamentos extends Component {
	componentDidMount() {
		this.props.fetchData();
	}

	render() {
		const { classes } = this.props;
		return (
			<Fragment>
				<Typography variant="h3" gutterBottom>
					Medicamentos
				</Typography>
				<Paper className={classes.root}>
					<Table className={classes.table}>
						<TableHead>
							<TableRow>
								<CustomTableCell />
								<CustomTableCell>ID</CustomTableCell>
								<CustomTableCell>Nombre</CustomTableCell>
								<CustomTableCell>Precio</CustomTableCell>
								<CustomTableCell>Stock</CustomTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{this.props.medicamentos.map((row, index) => {
								return (
									<TableRow className={classes.row} key={index}>
										<CustomTableCell component="th" scope="row">
											<Checkbox
												checked={row.checked}
												onChange={(e) => this.props.handleChange(row.id)}
											/>
										</CustomTableCell>
										<CustomTableCell component="th" scope="row">
											{row.id}
										</CustomTableCell>
										<CustomTableCell scope="row">{row.medName}</CustomTableCell>
										<CustomTableCell scope="row">{row.precio}</CustomTableCell>
										<CustomTableCell scope="row">{row.stock}</CustomTableCell>
									</TableRow>
								);
							})}
						</TableBody>
					</Table>
				</Paper>

				<Grid container className={classes.root} spacing={16}>
					<Grid item xs={12}>
						<Grid container className={classes.demo} justify="flex-end" spacing={8}>
							<Grid item>
								<CustomButtonPopover
									showText="Add"
									actionButton={this.props.handleClickOpen}
									iconButton={<AddIcon />}
								/>
							</Grid>
							<Grid item>
								<CustomButtonPopover showText="Edit" iconButton={<EditIcon />} />
							</Grid>
							<Grid item>
								<CustomButtonPopover showText="Delete" iconButton={<DeleteIcon />} />
							</Grid>
							<Grid item>
								<CustomButtonPopover showText="Save" iconButton={<SaveIcon />} />
							</Grid>
						</Grid>
					</Grid>
				</Grid>
				<NuevoMedicamento
					open={this.props.open}
					handleClose={this.props.handleClose}
					save={this.props.handleSave}
				/>
			</Fragment>
		);
	}
}

Medicamentos.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Medicamentos);
