const styles = (theme) => ({
	popover: {
		pointerEvents: 'none'
	},
	paper: {
		padding: theme.spacing.unit
	},
	button: {
		marginTop: theme.spacing.unit * 2
	}
});

export default styles;
