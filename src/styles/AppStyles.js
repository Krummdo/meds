import green from '@material-ui/core/colors/green';
import { createMuiTheme } from '@material-ui/core/styles';

const mainTheme = createMuiTheme({
	typography: {
		useNextVariants: true
	},
	palette: {
		primary: green,
		secondary: {
			main: '#00e676'
		},
		type: 'dark'
	}
});

export default mainTheme;
