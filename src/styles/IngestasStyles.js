import TableCell from '@material-ui/core/TableCell';
import { withStyles } from '@material-ui/core/styles';

const CustomTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: theme.palette.common.black,
		color: theme.palette.common.white,
		fontSize: 18
	},
	body: {
		fontSize: 16
	}
}))(TableCell);

const styles = (theme) => ({
	root: {
		width: '100%',
		marginTop: theme.spacing.unit,
		overflowX: 'auto'
	},
	table: {
		minWidth: 700
	},
	row: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.background.default
		}
	},
	button: {
		marginTop: theme.spacing.unit * 2
	},
	leftIcon: {
		marginRight: theme.spacing.unit
	},
	botonera: {
		justify: 'flex-end'
	}
});

export { CustomTableCell, styles };
