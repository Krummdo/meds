import React, { Component } from 'react';
import API from '../router/apiCentral';

export const MedicamentoContext = React.createContext();

export class MedicamentoProvider extends Component {
	constructor(props) {
		super(props);

		this.state = {
			medicamentos: [],
			open: false,
			medName: '',
			precio: 2000,
			stock: 0
		};

		this.fetchData = this.fetchData.bind(this);
	}

	fetchData() {
		const myApi = new API();
		myApi.createEntity({ name: 'medicamentos' });

		myApi.endpoints.medicamentos
			.getAll('')
			.then(({ data }) => {
				let medicamentos = data;
				this.setState({ medicamentos });
			})
			.catch((error) => console.log('skereeeeeeeeeeeeee', error));
	}

	handleClickOpen = () => {
		this.setState({ open: true });
	};

	handleClose = () => {
		this.setState({ open: false });
	};

	handleSave = () => {
		this.setState((prevState) => ({
			medicamentos: [ ...prevState.medicamentos, { id: 4, medName: 'asdasd', precio: 400, stock: 40 } ]
		}));
		this.handleClose();
	};

	handleChange = (rowId) =>
		this.setState({
			medicamentos: this.state.medicamentos.map((medicamento) => {
				if (medicamento.id === rowId) {
					return {
						...medicamento,
						checked: !medicamento.checked
					};
				}
				return medicamento;
			})
		});

	render() {
		return (
			<MedicamentoContext.Provider
				value={{
					...this.state,
					fetchData: this.fetchData,
					handleClickOpen: this.handleClickOpen,
					handleClose: this.handleClose,
					handleSave: this.handleSave,
					handleChange: this.handleChange
				}}
			>
				{this.props.children}
			</MedicamentoContext.Provider>
		);
	}
}
